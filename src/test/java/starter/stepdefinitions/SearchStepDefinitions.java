package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;
import starter.api.SearchAPI;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {
    @Steps
    public SearchAPI searchAPI;

    @When("he calls search endpoint with param: {string}")
    public void heCallsEndpoint(String searchTerm) {
        searchAPI.callEndpoint(searchTerm);
    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedForCola(String searchTerm) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response ->
                assertThat(response.extract().response().jsonPath().getList("title"),
                        hasItem(containsStringIgnoringCase(searchTerm))));
    }

    @Then("he does not see the results")
    public void he_Does_Not_See_The_Results() {
        restAssuredThat(response -> response.body("detail.error", Matchers.equalTo(true)));
    }

}
