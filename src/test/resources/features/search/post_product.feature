Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario: Searching for a product positive
    When he calls search endpoint with param: "apple"
    Then he sees the results displayed for "apple"
    When he calls search endpoint with param: "cola"
    Then he sees the results displayed for "cola"

  Scenario: Searching for a product negative
    When he calls search endpoint with param: "car"
    Then he does not see the results
