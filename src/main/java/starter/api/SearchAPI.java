package starter.api;

import starter.config.AppConfig;
import net.serenitybdd.rest.SerenityRest;

public class SearchAPI {
    private final String endpoint;

    public SearchAPI() {
        AppConfig appConfig = new AppConfig();
        String searchEndpoint = "/search/demo/";
        endpoint = appConfig.getApiEndpoint() + searchEndpoint;
    }

    public void callEndpoint(String searchTerm) {
        SerenityRest.given().get(endpoint + searchTerm);
    }
}