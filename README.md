# Instructions how to install, run and write new tests

As default API automation framework, it contains of three layers:
- API class, encapsulate API logic, for work with endpoints
- Step layer, contains reusable parts which describe user actions
- Tests actually business logic, which is build using previous layers

For open project you need to open pom.xml file and IDEA will load all rest dependencies.
As precondition for run test you need to install appropriate java version (Java 11).
To check Java version:

```json
$ java -version
```

### Run tests
 
After completed all precondition, we can run tests using mvn from command line:

```json
$ mvn clean verify
```

Afterwards using next command you can find serenity report index.html file in folder(target/site/serenity):

```json
mvn serenity:aggregate
```

### Write new tests
We use BDD approach, so you need to create new feature file with plain English test case
Implement Steps for each test case step if it required ot reuse already created steps
If required implement new API end point and use this code in steps layer.

# What was changed/refactored

- Removed unnecessary gradle dependency and files
- Introduced App config file, to move configuration for endpoint
- Implemented API class itself to move API calls logic from step layer
- Refactored step definition class, to take appropriate params and removed unnecessary params
- Add gitlab ci file, for configuration CI pipline (build, test, pages)
- After build artifacts can be dowloaded, and online access serenity report via pages link
